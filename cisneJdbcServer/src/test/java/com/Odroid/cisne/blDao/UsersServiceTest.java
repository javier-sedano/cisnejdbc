package com.Odroid.cisne.blDao;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.Odroid.cisne.bl.IUsersService;
import com.Odroid.cisne.domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"/com/Odroid/cisne/daoJdbc/spring-dao-context.xml",
		"/com/Odroid/cisne/blDao/spring-bl-context.xml",
		"/com/Odroid/cisne/blDao/spring-context.xml" })
@Transactional
public class UsersServiceTest {

	@Autowired
	@Qualifier("usersService")
	private IUsersService usersService;

	@Autowired
	@Qualifier("bcryptEncoder")
	private BCryptPasswordEncoder bcryptEncoder;

	@Test
	public void crudTest() {
		String username = "jsnow";
		String password = "qwerty";
		String name = "Jon Snow";
		Integer enabled = 1;
		User u = new User(-1L, username, password, enabled, name);
		Long id = usersService.addUser(u, true);
		Assert.assertTrue(usersService.isAdmin(username));
		usersService.saveUser(u, false);
		Assert.assertFalse(usersService.isAdmin(username));
		User u2 = usersService.getUser(id);
		Assert.assertEquals(username, u2.getUsername());
		Assert.assertEquals(name, u2.getName());
		Assert.assertEquals(enabled, u2.getEnabled());
		Assert.assertTrue(password + ": " + u2.getPassword(),
				bcryptEncoder.matches(password, u2.getPassword()));
		usersService.deleteUser(id);
	}

}
