package com.Odroid.cisne.daoJdbc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.Odroid.cisne.dao.INotesDao;
import com.Odroid.cisne.domain.Note;
import com.Odroid.cisne.fw.dao.FwDao;

@Repository("notesDao")
public class NotesJdbcDao extends FwDao implements INotesDao {
	private static String SQL_SEARCH = "NOTES_SEARCH";
	private static String SQL_ADD = "NOTES_ADD";
	private static String SQL_GET = "NOTES_GET";
	private static String SQL_SAVE = "NOTES_SAVE";
	private static String SQL_DELETE = "NOTES_DELETE";

	@Autowired
	@Qualifier("jdbcTemplate")
	private NamedParameterJdbcTemplate jdbcTemplate;

	@Override
	public List<Note> search(String text, Long userId) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("userId", userId);
		params.addValue("search", "%" + text + "%");
		List<Note> notes = jdbcTemplate.query(namedQuery(SQL_SEARCH), params,
				noteRowMapper);
		// Ejemplo de paginacion:
		// final int first = 3;
		// final int size = 4;
		// List<Note> notes = jdbcTemplate.query(namedQuery(SQL_SEARCH), params,
		// new ResultSetExtractor<List<Note>>() {
		// @Override
		// public List<Note> extractData(ResultSet rs)
		// throws SQLException, DataAccessException {
		// List<Note> res = new ArrayList<Note>();
		// try {
		// rs.absolute(first);
		// } catch (SQLFeatureNotSupportedException e) {
		// int moved = 0;
		// while (moved < first) {
		// rs.next();
		// moved++;
		// }
		// }
		// int nRows = 0;
		// while (rs.next() && (nRows < size)) {
		// System.out.println("-- " + nRows);
		// res.add(noteRowMapper.mapRow(rs, nRows));
		// nRows++;
		// }
		// return res;
		// }
		// });
		return notes;
	}

	@Override
	public Long add(Note note) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		SqlParameterSource params = new BeanPropertySqlParameterSource(note);
		jdbcTemplate.update(namedQuery(SQL_ADD), params, keyHolder);
		Long id = keyHolder.getKey().longValue();
		return id;
	}

	@Override
	public Note get(Long id) {
		SqlParameterSource params = new MapSqlParameterSource("id", id);
		Note note = jdbcTemplate.queryForObject(namedQuery(SQL_GET), params,
				noteRowMapper);
		return note;
	}

	@Override
	public void save(Note note) {
		SqlParameterSource params = new BeanPropertySqlParameterSource(note);
		jdbcTemplate.update(namedQuery(SQL_SAVE), params);
	}

	@Override
	public void delete(Long id) {
		SqlParameterSource params = new MapSqlParameterSource("id", id);
		jdbcTemplate.update(namedQuery(SQL_DELETE), params);
	}

	private RowMapper<Note> noteRowMapper = new BeanPropertyRowMapper<>(
			Note.class, true);
}
