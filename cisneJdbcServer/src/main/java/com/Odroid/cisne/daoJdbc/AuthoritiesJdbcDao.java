package com.Odroid.cisne.daoJdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.Odroid.cisne.dao.IAuthoritiesDao;
import com.Odroid.cisne.fw.dao.FwDao;

@Repository("authoritiesDao")
public class AuthoritiesJdbcDao extends FwDao implements IAuthoritiesDao {
	private static String SQL_ADD = "AUTHORITIES_ADD";
	private static String SQL_DELETE = "AUTHORITIES_DELETE";
	private static String SQL_DELETEALL = "AUTHORITIES_DELETEALL";
	private static String SQL_HAS = "AUTHORITIES_HAS";

	@Autowired
	@Qualifier("jdbcTemplate")
	private NamedParameterJdbcTemplate jdbcTemplate;

	@Override
	public void addAuthority(String username, String authority) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("username", username);
		params.addValue("authority", authority);
		jdbcTemplate.update(namedQuery(SQL_ADD), params);
	}

	@Override
	public void removeAuthority(String username, String authority) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("username", username);
		params.addValue("authority", authority);
		jdbcTemplate.update(namedQuery(SQL_DELETE), params);
	}

	@Override
	public void removeAllAuthorities(String username) {
		MapSqlParameterSource params = new MapSqlParameterSource("username",
				username);
		jdbcTemplate.update(namedQuery(SQL_DELETEALL), params);
	}

	@Override
	public boolean hasAuthority(String username, String authority) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("username", username);
		params.addValue("authority", authority);
		Long count = jdbcTemplate.queryForObject(namedQuery(SQL_HAS), params,
				Long.class);
		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}
}
