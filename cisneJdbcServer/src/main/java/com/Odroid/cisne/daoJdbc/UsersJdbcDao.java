package com.Odroid.cisne.daoJdbc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.Odroid.cisne.dao.IUsersDao;
import com.Odroid.cisne.domain.User;
import com.Odroid.cisne.fw.dao.FwDao;

@Repository("usersDao")
public class UsersJdbcDao extends FwDao implements IUsersDao {
	private static String SQL_LIST = "USERS_LIST";
	private static String SQL_ADD = "USERS_ADD";
	private static String SQL_GETBYID = "USERS_GETBYID";
	private static String SQL_GETBYUSERNAME = "USERS_GETBYUSERNAME";
	private static String SQL_SAVE = "USERS_SAVE";
	private static String SQL_DELETE = "USERS_DELETE";

	@Autowired
	@Qualifier("jdbcTemplate")
	private NamedParameterJdbcTemplate jdbcTemplate;

	@Override
	public List<User> listUsers() {
		List<User> res = jdbcTemplate
				.query(namedQuery(SQL_LIST), userRowMapper);
		return res;
	}

	@Override
	public User getUser(long id) {
		SqlParameterSource params = new MapSqlParameterSource("id", id);
		User res = jdbcTemplate.queryForObject(namedQuery(SQL_GETBYID), params,
				userRowMapper);
		return res;
	}

	@Override
	public User getUser(String username) {
		SqlParameterSource params = new MapSqlParameterSource("username",
				username);
		User res = jdbcTemplate.queryForObject(namedQuery(SQL_GETBYUSERNAME),
				params, userRowMapper);
		return res;
	}

	@Override
	public Long addUser(User user) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		SqlParameterSource params = new BeanPropertySqlParameterSource(user);
		jdbcTemplate.update(namedQuery(SQL_ADD), params, keyHolder);
		Long id = keyHolder.getKey().longValue();
		return id;
	}

	@Override
	public void saveUser(User user) {
		SqlParameterSource params = new BeanPropertySqlParameterSource(user);
		jdbcTemplate.update(namedQuery(SQL_SAVE), params);
	}

	@Override
	public void deleteUser(long id) {
		SqlParameterSource params = new MapSqlParameterSource("id", id);
		jdbcTemplate.update(namedQuery(SQL_DELETE), params);
	}

	private RowMapper<User> userRowMapper = new BeanPropertyRowMapper<>(
			User.class, true);
}
