package com.Odroid.cisne.dao;

public interface IAuthoritiesDao {
	public void addAuthority(String username, String authority);

	public void removeAuthority(String username, String authority);

	public void removeAllAuthorities(String username);
	
	public boolean hasAuthority(String username, String authority);

}
