package com.Odroid.cisne.dao;

import java.util.List;

import com.Odroid.cisne.domain.User;

public interface IUsersDao {
	public List<User> listUsers();

	public User getUser(long id);

	public User getUser(String username);

	public Long addUser(User user);

	public void saveUser(User user);

	public void deleteUser(long id);

}
