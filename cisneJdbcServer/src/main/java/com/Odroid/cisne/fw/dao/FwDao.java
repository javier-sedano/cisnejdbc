package com.Odroid.cisne.fw.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.Odroid.cisne.fw.dao.NamedQueryResolver.NamedQueryNotDefinedException;

public class FwDao {

	@Autowired
	@Qualifier("namedQueryResolver")
	private NamedQueryResolver namedQueryResolver;

	protected String namedQuery(String name)
			throws NamedQueryNotDefinedException {
		return namedQueryResolver.namedQuery(name);
	}

}
