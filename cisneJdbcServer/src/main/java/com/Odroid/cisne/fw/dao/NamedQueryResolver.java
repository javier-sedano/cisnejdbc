package com.Odroid.cisne.fw.dao;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("namedQueryResolver")
public class NamedQueryResolver {
	@Autowired
	@Qualifier("sqlQueries")
	private Properties sqlQueries;

	public String namedQuery(String name) throws NamedQueryNotDefinedException {
		String query = sqlQueries.getProperty(name);
		if (query == null) {
			throw new NamedQueryNotDefinedException(name + " not defined");
		}
		return query;
	}

	public static class NamedQueryNotDefinedException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public NamedQueryNotDefinedException(String message) {
			super(message);
		}
	}

}
