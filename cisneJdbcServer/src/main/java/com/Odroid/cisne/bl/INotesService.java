package com.Odroid.cisne.bl;

import java.util.List;

import com.Odroid.cisne.domain.Note;

public interface INotesService {
	public List<Note> search(String text, Long userId);
	
	public Long add(Note note);
	
	public Note get(Long id);
	
	public void save(Note note);
	
	public void delete(Long id);
}
