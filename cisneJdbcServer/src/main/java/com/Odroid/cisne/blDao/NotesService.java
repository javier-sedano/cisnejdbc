package com.Odroid.cisne.blDao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.method.P;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.Odroid.cisne.bl.INotesService;
import com.Odroid.cisne.dao.INotesDao;
import com.Odroid.cisne.domain.Note;

@Service("notesService")
@Transactional(readOnly = true)
public class NotesService implements INotesService {

	@Autowired
	@Qualifier("notesDao")
	private INotesDao notesDao;

	@Override
	public List<Note> search(String text, Long userId) {
		return notesDao.search(text, userId);
	}

	@Transactional(readOnly = false)
	@Override
	@PreAuthorize("#note.userId == principal.id")
	public Long add(@P("note") Note note) {
		return notesDao.add(note);
	}

	@Override
	@PostAuthorize("principal.id == returnObject.userId")
	public Note get(Long id) {
		Note note = notesDao.get(id);
		return note;
	}

	@Transactional(readOnly = false)
	@Override
	@PreAuthorize("@notesDao.get(#note.id).userId == principal.id")
	public void save(@P("note") Note note) {
		notesDao.save(note);
	}

	@Transactional(readOnly = false)
	@Override
	@PreAuthorize("@notesDao.get(#id).userId == principal.id")
	public void delete(@P("id")Long id) {
		notesDao.delete(id);
	}

}
