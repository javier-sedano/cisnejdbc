package com.Odroid.cisne.blDao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.Odroid.cisne.bl.IUsersService;
import com.Odroid.cisne.dao.IAuthoritiesDao;
import com.Odroid.cisne.dao.IUsersDao;
import com.Odroid.cisne.domain.User;
import com.Odroid.cisne.fw.bl.FwSession;

@Service("usersService")
@Transactional(readOnly = true)
public class UsersService implements IUsersService {
	private static String ROLE_ADMIN = FwSession.ROLE_ADMIN;
	private static String ROLE_USER = FwSession.ROLE_USER;

	@Autowired
	@Qualifier("usersDao")
	private IUsersDao usersDao;

	@Autowired
	@Qualifier("authoritiesDao")
	private IAuthoritiesDao authoritiesDao;

	@Autowired
	@Qualifier("bcryptEncoder")
	private BCryptPasswordEncoder passwordEncoder;

	@Override
	public List<User> getUsers() {
		List<User> users = usersDao.listUsers();
		return users;
	}

	@Override
	public User getUser(long id) {
		return usersDao.getUser(id);
	}

	@Override
	public User getUser(String username) {
		return usersDao.getUser(username);
	}

	@Override
	public boolean isAdmin(String username) {
		if (authoritiesDao.hasAuthority(username, ROLE_ADMIN)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Long addUser(User user, boolean admin) {
		if (user.getPassword().isEmpty()) {
			throw new EmptyPasswordException();
		}
		String encodedPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(encodedPassword);
		Long userId = usersDao.addUser(user);
		authoritiesDao.addAuthority(user.getUsername(), ROLE_USER);
		if (admin) {
			authoritiesDao.addAuthority(user.getUsername(), ROLE_ADMIN);
		}
		return userId;
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public void saveUser(User user, boolean admin) {
		if (user.getPassword().isEmpty()) {
			User currentUser = usersDao.getUser(user.getId());
			user.setPassword(currentUser.getPassword());
		} else {
			String encodedPassword = passwordEncoder.encode(user.getPassword());
			user.setPassword(encodedPassword);
		}
		usersDao.saveUser(user);
		if (!authoritiesDao.hasAuthority(user.getUsername(), ROLE_USER)) {
			authoritiesDao.addAuthority(user.getUsername(), ROLE_USER);
		}
		if (admin) {
			if (!authoritiesDao.hasAuthority(user.getUsername(), ROLE_ADMIN)) {
				authoritiesDao.addAuthority(user.getUsername(), ROLE_ADMIN);
			}
		} else {
			if (authoritiesDao.hasAuthority(user.getUsername(), ROLE_ADMIN)) {
				authoritiesDao.removeAuthority(user.getUsername(), ROLE_ADMIN);
			}
		}
	}

	@Override
	@Transactional(readOnly = false)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public void deleteUser(long id) {
		User user = usersDao.getUser(id);
		authoritiesDao.removeAllAuthorities(user.getUsername());
		usersDao.deleteUser(id);
	}

}
