<%@ include file="../bits/html-head.jsp"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="<c:url value="/res/js/notes.js"/>"></script>

<div id="main">
	<%@ include file="../bits/header.jsp"%>

	<div id="site_content">
		<div class="leftbar">
			<h3>
				<spring:message code="notes.list.header" />
			</h3>
			<a href="#" onclick="notes.refreshList(); return false;"
				class="buttonImg"> <img
				src="<c:url value="/res/img/refresh.png"/>"
				alt="<spring:message code="notes.list.refresh" />"
				title="<spring:message code="notes.list.refresh" />" />
			</a> <a href="#" onclick="notes.cleanFilter(); return false;"
				class="buttonImg"> <img
				src="<c:url value="/res/img/clean.png"/>"
				alt="<spring:message code="notes.list.clean" />"
				title="<spring:message code="notes.list.clean" />" />
			</a> <a href="#" onclick="notes.add(); return false;" class="buttonImg">
				<img src="<c:url value="/res/img/add.png"/>"
				alt="<spring:message code="notes.list.add" />"
				title="<spring:message code="notes.list.add" />" />
			</a><br /> <input id="notes_filter" class="notesSearchTextfield" />
			<div id="notes_list"></div>
		</div>
		<div id="content">
			<h1>
				<spring:message code="notes.detail.header" />
			</h1>
			<div id="notes_details_empty" style="display: none">
				<p>
					<spring:message code="notes.detail.empty" />
				</p>
			</div>
			<div id="notes_details">
				<p>
					<spring:message code="notes.detail.empty" />
				</p>
			</div>
		</div>
	</div>

</div>

<%@ include file="../bits/html-foot.jsp"%>