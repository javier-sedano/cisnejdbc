<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Cisne</title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/res/css/shadow-play-1/style.css"/>">
<link rel="stylesheet" type="text/css"
  href="<c:url value="/res/css/jquery-ui/jquery-ui.css"/>">
<link rel="stylesheet" type="text/css"
  href="<c:url value="/res/css/common.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/res/css/notes.css"/>">
<script type="text/javascript"
	src="<c:url value="/res/js/jquery-1.7.2.js"/>"></script>
<script type="text/javascript" src="<c:url value="/res/js/jquery-ui.js"/>"></script>
<script type="text/javascript" src="<c:url value="/res/js/fw.js"/>"></script>
<script type="text/javascript" src="<c:url value="/res/js/common.js"/>"></script>
<script type="text/javascript">
	var context = <c:url value="/"/>;
	var url_res = context + "res/";
	var url_public = context + "public/";
</script>
</head>
<body>