<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<div id="header">
	<div id="menubar">
		<ul id="menu">

			<c:choose>
				<c:when test="${tab=='notes'}">
					<li class="selected"><a href="<c:url value="/notes.html"/>"><spring:message
								code="common.tab.notes" /></a></li>
				</c:when>
				<c:otherwise>
					<li><a href="<c:url value="/notes.html"/>"><spring:message
								code="common.tab.notes" /></a></li>
				</c:otherwise>
			</c:choose>

			<sec:authorize access="hasRole('ROLE_ADMIN')">
				<c:choose>
					<c:when test="${tab=='users'}">
						<li class="selected"><a href="<c:url value="/users.html"/>"><spring:message
									code="common.tab.users" /></a></li>
					</c:when>
					<c:otherwise>
						<li><a href="<c:url value="/users.html"/>"><spring:message
									code="common.tab.users" /></a></li>
					</c:otherwise>
				</c:choose>
			</sec:authorize>

			<sec:authentication property="principal.username" var="username" scope="request"/>
			<li><a href="<c:url value="/logout.html"/>"> <spring:message
						code="common.tab.logout" arguments="${username}" />
			</a></li>

		</ul>
	</div>
</div>

